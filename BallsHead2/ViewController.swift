//
//  ViewController.swift
//  BallsHead2
//
//  Created by Rueben Hales on 11/2/22.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        
        let newballshead = BallsHead(context: context)
        newballshead.name = "BallsHead #1192"
        newballshead.owner = "QLX_NFT"
        newballshead.picture = UIImage(named: "#1192")!.jpegData(compressionQuality:1.0)!
        newballshead.price = 0.050
        try? context.save()
        
        retrieveCollection()
        
    }
   
    
    func retrieveCollection(){
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://api.opensea.io/api/v1/collection/ballshead-v2")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error as Any)
            } else {
                let data = try? JSONDecoder().decode(Ballsheadcollection.self, from: data!)
                dump(data!)
            }
        })
        
        dataTask.resume()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func ballshead(_ sender: UIButton) {
        UIApplication.shared.open(URL(string:"https://ballshead.com/")!
                                  as URL, options: [:], completionHandler: nil)
        
        
        
        
        
    }
    
    @IBAction func ballsheadcom(_ sender: Any) {
        UIApplication.shared.open(URL(string:"https://ballshead.com/")!
                                  as URL, options: [:], completionHandler: nil)
        
    }
    @IBAction func twitter(_ sender: Any) {
        UIApplication.shared.open(URL(string:"https://twitter.com/BallsHeadNFT")!
                                  as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func instagram(_ sender: Any) {
        UIApplication.shared.open(URL(string:"https://www.instagram.com/ballsheadnft/")!
                                  as URL, options: [:], completionHandler: nil)
    }
    
    @IBAction func youtube(_ sender: Any) {
        UIApplication.shared.open(URL(string:"https://www.youtube.com/channel/UCydG2cDiwgw5pRw60nKn7Rw")!
                                  as URL, options: [:], completionHandler: nil)
    }
    
   @IBAction func discord(_ sender: Any) {
        UIApplication.shared.open(URL(string:"https://discord.com/invite/Ma8EzkTpgh")!
                                  as URL, options: [:], completionHandler: nil)
  }
}
