// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let ballsheadcollection = try? newJSONDecoder().decode(Ballsheadcollection.self, from: jsonData)

import Foundation

// MARK: - Ballsheadcollection
struct Ballsheadcollection: Codable {
    let collection: Collection
}

// MARK: - Collection
struct Collection: Codable { 
    let editors: [String]
    let paymentTokens: [PaymentToken]
    let primaryAssetContracts: [PrimaryAssetContract]
    let traits: Traits
    let stats: [String: Int?]
    let bannerImageURL: String
    let chatURL: JSONNull?
    let createdDate: String
    let defaultToFiat: Bool
    let collectionDescription, devBuyerFeeBasisPoints, devSellerFeeBasisPoints: String
    let discordURL: JSONNull?
    let displayData: DisplayData
    let externalURL: JSONNull?
    let featured: Bool
    let featuredImageURL: String
    let hidden: Bool
    let safelistRequestStatus: String
    let imageURL: String
    let isSubjectToWhitelist: Bool
    let largeImageURL: String
    let mediumUsername: JSONNull?
    let name: String
    let onlyProxiedTransfers: Bool
    let openseaBuyerFeeBasisPoints, openseaSellerFeeBasisPoints: String
    let payoutAddress: JSONNull?
    let requireEmail: Bool
    let shortDescription: JSONNull?
    let slug: String
    let telegramURL, twitterUsername, instagramUsername, wikiURL: JSONNull?
    let isNsfw: Bool
    let fees: Fees
    let isRarityEnabled: Bool

    enum CodingKeys: String, CodingKey {
        case editors
        case paymentTokens = "payment_tokens"
        case primaryAssetContracts = "primary_asset_contracts"
        case traits, stats
        case bannerImageURL = "banner_image_url"
        case chatURL = "chat_url"
        case createdDate = "created_date"
        case defaultToFiat = "default_to_fiat"
        case collectionDescription = "description"
        case devBuyerFeeBasisPoints = "dev_buyer_fee_basis_points"
        case devSellerFeeBasisPoints = "dev_seller_fee_basis_points"
        case discordURL = "discord_url"
        case displayData = "display_data"
        case externalURL = "external_url"
        case featured
        case featuredImageURL = "featured_image_url"
        case hidden
        case safelistRequestStatus = "safelist_request_status"
        case imageURL = "image_url"
        case isSubjectToWhitelist = "is_subject_to_whitelist"
        case largeImageURL = "large_image_url"
        case mediumUsername = "medium_username"
        case name
        case onlyProxiedTransfers = "only_proxied_transfers"
        case openseaBuyerFeeBasisPoints = "opensea_buyer_fee_basis_points"
        case openseaSellerFeeBasisPoints = "opensea_seller_fee_basis_points"
        case payoutAddress = "payout_address"
        case requireEmail = "require_email"
        case shortDescription = "short_description"
        case slug
        case telegramURL = "telegram_url"
        case twitterUsername = "twitter_username"
        case instagramUsername = "instagram_username"
        case wikiURL = "wiki_url"
        case isNsfw = "is_nsfw"
        case fees
        case isRarityEnabled = "is_rarity_enabled"
    }
}

// MARK: - DisplayData
struct DisplayData: Codable {
    let cardDisplayStyle: String
    let images: JSONNull?

    enum CodingKeys: String, CodingKey {
        case cardDisplayStyle = "card_display_style"
        case images
    }
}

// MARK: - Fees
struct Fees: Codable {
    let sellerFees: SellerFees
    let openseaFees: OpenseaFees

    enum CodingKeys: String, CodingKey {
        case sellerFees = "seller_fees"
        case openseaFees = "opensea_fees"
    }
}

// MARK: - OpenseaFees
struct OpenseaFees: Codable {
    let the0X0000A26B00C1F0Df003000390027140000Faa719: Int

    enum CodingKeys: String, CodingKey {
        case the0X0000A26B00C1F0Df003000390027140000Faa719 = "0x0000a26b00c1f0df003000390027140000faa719"
    }
}

// MARK: - SellerFees
struct SellerFees: Codable {
    let the0X62149Cc323957B698Ff09D3Ab75Abbd54B6Fe473: Int

    enum CodingKeys: String, CodingKey {
        case the0X62149Cc323957B698Ff09D3Ab75Abbd54B6Fe473 = "0x62149cc323957b698ff09d3ab75abbd54b6fe473"
    }
}

// MARK: - PaymentToken
struct PaymentToken: Codable {
    let id: Int
    let symbol, address: String
    let imageURL: String
    let name: String
    let decimals, ethPrice: Int
    let usdPrice: Double

    enum CodingKeys: String, CodingKey {
        case id, symbol, address
        case imageURL = "image_url"
        case name, decimals
        case ethPrice = "eth_price"
        case usdPrice = "usd_price"
    }
}

// MARK: - PrimaryAssetContract
struct PrimaryAssetContract: Codable {
    let address, assetContractType, createdDate, name: String
    let nftVersion: String
    let openseaVersion: JSONNull?
    let owner: Int
    let schemaName, symbol, totalSupply, primaryAssetContractDescription: String
    let externalLink: JSONNull?
    let imageURL: String
    let defaultToFiat: Bool
    let devBuyerFeeBasisPoints, devSellerFeeBasisPoints: Int
    let onlyProxiedTransfers: Bool
    let openseaBuyerFeeBasisPoints, openseaSellerFeeBasisPoints, buyerFeeBasisPoints, sellerFeeBasisPoints: Int
    let payoutAddress: JSONNull?

    enum CodingKeys: String, CodingKey {
        case address
        case assetContractType = "asset_contract_type"
        case createdDate = "created_date"
        case name
        case nftVersion = "nft_version"
        case openseaVersion = "opensea_version"
        case owner
        case schemaName = "schema_name"
        case symbol
        case totalSupply = "total_supply"
        case primaryAssetContractDescription = "description"
        case externalLink = "external_link"
        case imageURL = "image_url"
        case defaultToFiat = "default_to_fiat"
        case devBuyerFeeBasisPoints = "dev_buyer_fee_basis_points"
        case devSellerFeeBasisPoints = "dev_seller_fee_basis_points"
        case onlyProxiedTransfers = "only_proxied_transfers"
        case openseaBuyerFeeBasisPoints = "opensea_buyer_fee_basis_points"
        case openseaSellerFeeBasisPoints = "opensea_seller_fee_basis_points"
        case buyerFeeBasisPoints = "buyer_fee_basis_points"
        case sellerFeeBasisPoints = "seller_fee_basis_points"
        case payoutAddress = "payout_address"
    }
}

// MARK: - Traits
struct Traits: Codable {
    let team: [String: Int]
    let type: TypeClass
    let subType: SubType
    let job: Job
    let position: Position

    enum CodingKeys: String, CodingKey {
        case team, type
        case subType = "sub type"
        case job, position
    }
}

// MARK: - Job
struct Job: Codable {
    let player, coach: Int
}

// MARK: - Position
struct Position: Codable {
    let defender, forward, midfielder, goalkeeper: Int
    let bench: Int
}

// MARK: - SubType
struct SubType: Codable {
    let non, horror: Int
}

// MARK: - TypeClass
struct TypeClass: Codable {
    let humanoid, impersonator, horror: Int
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

