//
//  BallsHead+CoreDataProperties.swift
//  BallsHead2
//
//  Created by Ruben Hales on 12/7/22.
//
//

import Foundation
import CoreData


extension BallsHead {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BallsHead> {
        return NSFetchRequest<BallsHead>(entityName: "BallsHead")
    }

    @NSManaged public var name: String?
    @NSManaged public var price: Double
    @NSManaged public var picture: Data?
    @NSManaged public var owner: String?
    @NSManaged public var properties: String?

}

extension BallsHead : Identifiable {

}
